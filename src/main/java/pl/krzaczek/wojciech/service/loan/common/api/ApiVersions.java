package pl.krzaczek.wojciech.service.loan.common.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ApiVersions {

    public static final String V1_JSON = "application/vnd.loan-service.v1+json";
}

package pl.krzaczek.wojciech.service.loan.common.api;

import lombok.Value;

@Value
class ValidationExceptionEntry {

    String key;
    String reason;
}

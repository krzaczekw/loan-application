package pl.krzaczek.wojciech.service.loan.loan.dto.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static pl.krzaczek.wojciech.service.loan.common.api.ExceptionsCodes.PAST_DATE;

@Retention(RUNTIME)
@Target({FIELD})
@Constraint(validatedBy = NotPastDateValidator.class)
public @interface NotPastDate {
    String message() default PAST_DATE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

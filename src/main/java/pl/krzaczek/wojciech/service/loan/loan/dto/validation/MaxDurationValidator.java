package pl.krzaczek.wojciech.service.loan.loan.dto.validation;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MaxDurationValidator implements ConstraintValidator<MaxDuration, Integer> {

    @Value("${services.loan.validation.duration.max}")
    private Integer maximalDuration;

    @Override
    public boolean isValid(Integer duration, ConstraintValidatorContext constraintValidatorContext) {
        return duration == null || duration <= maximalDuration;
    }

}

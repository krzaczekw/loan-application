package pl.krzaczek.wojciech.service.loan.loan.dto.validation;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.krzaczek.wojciech.service.loan.common.service.CurrentTimeProvider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

@Slf4j
@RequiredArgsConstructor
public class NotPastDateValidator implements ConstraintValidator<NotPastDate, LocalDate> {

    private final CurrentTimeProvider currentTimeProvider;

    @Override
    public boolean isValid(LocalDate localDate, ConstraintValidatorContext context) {
        if (localDate == null) {
            return true;
        }
        return !currentTimeProvider.getLocalDate().isAfter(localDate);
    }
}

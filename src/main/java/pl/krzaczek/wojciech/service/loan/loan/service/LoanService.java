package pl.krzaczek.wojciech.service.loan.loan.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.krzaczek.wojciech.service.loan.loan.dto.LoanRequest;
import pl.krzaczek.wojciech.service.loan.loan.model.Loan;
import pl.krzaczek.wojciech.service.loan.loan.service.exception.LoanAlreadyProlongatedException;
import pl.krzaczek.wojciech.service.loan.loan.service.exception.LoanNotFoundException;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class LoanService {
    private final LoanRepository loanRepository;
    private final LoanFactory loanFactory;

    @Value("${services.loan.validation.prolongation}")
    private Integer prolongation;

    @Value("${services.loan.validation.interest}")
    private BigDecimal interest;

    public Long createLoan(LoanRequest loanRequest) {
        BigDecimal repaymentAmount = loanRequest.getAmount()
                .multiply(BigDecimal.ONE.add(interest));
        Loan s = loanFactory.create(loanRequest, repaymentAmount);
        return loanRepository.save(s).getId();
    }

    @Transactional
    public LocalDate prolongateLoan(Long id) {
        Loan loan = loanRepository.findById(id)
                .orElseThrow(() -> new LoanNotFoundException(String.format("Loan with id: %d not found", id)));
        if (wasNotProlongated(loan)) {
            loan.setProlongated(true);
            loan.setDuration(loan.getDuration() + prolongation);
            loanRepository.save(loan);
            return loan.getDate().plusDays(loan.getDuration());
        }
        throw new LoanAlreadyProlongatedException(String.format("Loan with id: %d was already prolongated", id));
    }

    private boolean wasNotProlongated(Loan loan) {
        return Boolean.FALSE.equals(loan.getProlongated());
    }
}

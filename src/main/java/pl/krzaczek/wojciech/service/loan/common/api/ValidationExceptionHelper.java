package pl.krzaczek.wojciech.service.loan.common.api;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

@Component
public class ValidationExceptionHelper {

    Set<ValidationExceptionEntry> extractValidationExceptionList(BindingResult bindingResult) {
        Set<ValidationExceptionEntry> validationErrors = new HashSet<>();
        fillValidationExceptionList(FieldError::getField, bindingResult.getFieldErrors(), validationErrors);
        fillValidationExceptionList(ObjectError::getObjectName, bindingResult.getGlobalErrors(),
                validationErrors);
        return validationErrors;
    }

    private <T extends ObjectError> void fillValidationExceptionList(Function<T, String> keyExtractionFunction,
                                                                     List<T> errors, Set<ValidationExceptionEntry> validationErrors) {
        errors.stream()
                .map(objectError -> new ValidationExceptionEntry(keyExtractionFunction.apply(objectError),
                        objectError.getDefaultMessage())).forEach(validationErrors::add);
    }
}

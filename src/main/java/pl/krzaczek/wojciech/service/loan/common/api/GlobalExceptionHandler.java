package pl.krzaczek.wojciech.service.loan.common.api;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.krzaczek.wojciech.service.loan.common.service.CurrentTimeProvider;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private static final String VALIDATION_ERROR_KEY = "Validation error";
    private final CurrentTimeProvider currentTimeProvider;
    private final ValidationExceptionHelper validationExceptionHelper;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity handleMethodArgumentNotValidException(HttpServletRequest req, MethodArgumentNotValidException argumentNotValidException) {
        return prepareResponse(argumentNotValidException.getBindingResult(), req, argumentNotValidException);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity handleBindException(HttpServletRequest req, BindException bindException) {
        return prepareResponse(bindException.getBindingResult(), req, bindException);
    }

    private ResponseEntity prepareResponse(BindingResult bindingResult, HttpServletRequest req, Exception ex) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("timestamp", currentTimeProvider.getLocalDateTime());
        map.put("status", HttpStatus.BAD_REQUEST.value());
        map.put("error", VALIDATION_ERROR_KEY);
        map.put("exception", ex.getClass().getName());
        map.put("message", validationExceptionHelper.extractValidationExceptionList(bindingResult));
        map.put("path", req.getRequestURI());
        return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    }

}

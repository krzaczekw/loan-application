package pl.krzaczek.wojciech.service.loan.common.service;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Component
public class CurrentTimeProvider {

    public LocalDateTime getLocalDateTime() {
        return LocalDateTime.now();
    }

    public LocalTime getLocalTime() {
        return LocalTime.now();
    }

    public LocalDate getLocalDate() {
        return LocalDate.now();
    }
}

package pl.krzaczek.wojciech.service.loan.loan.service;


import org.springframework.data.jpa.repository.JpaRepository;
import pl.krzaczek.wojciech.service.loan.loan.model.Loan;

public interface LoanRepository extends JpaRepository<Loan, Long> {
}

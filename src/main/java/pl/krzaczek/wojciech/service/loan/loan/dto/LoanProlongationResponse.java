package pl.krzaczek.wojciech.service.loan.loan.dto;

import lombok.Value;

import java.time.LocalDate;

@Value
public class LoanProlongationResponse {
    LocalDate repaymentDeadline;
}

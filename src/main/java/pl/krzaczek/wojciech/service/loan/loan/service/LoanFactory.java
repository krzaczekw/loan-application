package pl.krzaczek.wojciech.service.loan.loan.service;

import org.springframework.stereotype.Component;
import pl.krzaczek.wojciech.service.loan.loan.dto.LoanRequest;
import pl.krzaczek.wojciech.service.loan.loan.model.Loan;

import java.math.BigDecimal;

@Component
public class LoanFactory {

    public Loan create(LoanRequest loanRequest, BigDecimal repaymentAmount) {
        return new Loan(loanRequest.getAmount(),
                repaymentAmount,
                loanRequest.getDuration(),
                loanRequest.getDate());
    }
}

package pl.krzaczek.wojciech.service.loan.loan;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.krzaczek.wojciech.service.loan.loan.dto.LoanProlongationResponse;
import pl.krzaczek.wojciech.service.loan.loan.dto.LoanRequest;
import pl.krzaczek.wojciech.service.loan.loan.service.LoanService;

import javax.validation.Valid;
import java.net.URI;

import static pl.krzaczek.wojciech.service.loan.common.api.ApiVersions.V1_JSON;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/loan")
public class LoanController {

    private final LoanService loanService;

    @PostMapping(consumes = V1_JSON)
    public ResponseEntity createLoan(@RequestBody @Valid LoanRequest loanRequest) {
        log.info("[START][LOAN][CREATE]Creating loan: {}", loanRequest);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(loanService.createLoan(loanRequest)).toUri();
        log.info("[FINISH][LOAN][CREATE]Created loan with id: {}", loanRequest);
        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/{id}/prolongation", produces = V1_JSON)
    public LoanProlongationResponse prolongateLoan(@PathVariable("id") Long id) {
        log.info("[START][LOAN][CREATE]Prolongate loan: {}", id);
        LoanProlongationResponse loanProlongationResponse = new LoanProlongationResponse(loanService.prolongateLoan(id));
        log.info("[FINISH][LOAN][CREATE]Prolongated loan: {}", loanProlongationResponse);
        return loanProlongationResponse;
    }

}

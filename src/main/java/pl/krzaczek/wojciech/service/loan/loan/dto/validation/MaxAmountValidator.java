package pl.krzaczek.wojciech.service.loan.loan.dto.validation;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.krzaczek.wojciech.service.loan.common.service.CurrentTimeProvider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

@Component
@RequiredArgsConstructor
public class MaxAmountValidator implements ConstraintValidator<MaxAmount, BigDecimal> {

    private final CurrentTimeProvider currentTimeProvider;
    @Value("${services.loan.validation.amount.max}")
    private BigDecimal maximalAmount;
    @Value("${services.loan.validation.amount.max-invalidation.from}")
    private Integer maxInvalidationFromHour;
    @Value("${services.loan.validation.amount.max-invalidation.to}")
    private Integer maxInvalidationToHour;

    @Override
    public boolean isValid(BigDecimal amount, ConstraintValidatorContext constraintValidatorContext) {
        if (amount == null) {
            return true;
        }
        if (amount.compareTo(maximalAmount) < 0) {
            return true;
        }
        int currentHour = currentTimeProvider.getLocalTime().getHour();
        return amount.compareTo(maximalAmount) == 0
                && (currentHour < maxInvalidationFromHour || currentHour >= maxInvalidationToHour);
    }

}

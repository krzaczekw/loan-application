package pl.krzaczek.wojciech.service.loan.loan.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
@Table
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private BigDecimal amount;
    @Column(nullable = false)
    private BigDecimal repaymentAmount;
    @Column(nullable = false)
    private Integer duration;
    @Column(nullable = false)
    private Boolean prolongated = false;
    @Column(nullable = false)
    private LocalDate date;

    public Loan(BigDecimal amount, BigDecimal repaymentAmount, Integer duration, LocalDate date) {
        this.amount = amount;
        this.repaymentAmount = repaymentAmount;
        this.duration = duration;
        this.date = date;
    }
}

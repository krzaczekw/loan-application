package pl.krzaczek.wojciech.service.loan.common.api;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class ExceptionsCodes {
    public static final String REQUIRED = "REQUIRED";
    public static final String TOO_LOW = "TOO_LOW";
    public static final String TOO_BIG = "TOO_BIG";
    public static final String PAST_DATE = "PAST_DATE";
}

package pl.krzaczek.wojciech.service.loan.loan.dto;

import lombok.Value;
import pl.krzaczek.wojciech.service.loan.loan.dto.validation.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

import static pl.krzaczek.wojciech.service.loan.common.api.ExceptionsCodes.REQUIRED;

@Value
public class LoanRequest {

    @MinAmount
    @MaxAmount
    @NotNull(message = REQUIRED)
    BigDecimal amount;

    @MinDuration
    @MaxDuration
    @NotNull(message = REQUIRED)
    Integer duration;

    @NotPastDate
    @NotNull(message = REQUIRED)
    LocalDate date;
}

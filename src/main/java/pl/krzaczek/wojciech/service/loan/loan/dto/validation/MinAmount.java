package pl.krzaczek.wojciech.service.loan.loan.dto.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static pl.krzaczek.wojciech.service.loan.common.api.ExceptionsCodes.TOO_LOW;

@Retention(RUNTIME)
@Target({FIELD})
@Constraint(validatedBy = MinAmountValidator.class)
public @interface MinAmount {
    String message() default TOO_LOW;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

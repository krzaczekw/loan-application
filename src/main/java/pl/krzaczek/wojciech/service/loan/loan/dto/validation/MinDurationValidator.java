package pl.krzaczek.wojciech.service.loan.loan.dto.validation;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MinDurationValidator implements ConstraintValidator<MinDuration, Integer> {

    @Value("${services.loan.validation.duration.min}")
    private Integer minimalDuration;

    @Override
    public boolean isValid(Integer duration, ConstraintValidatorContext constraintValidatorContext) {
        return duration == null || duration >= minimalDuration;
    }
}

package pl.krzaczek.wojciech.service.loan.loan.dto.validation;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class MinAmountValidator implements ConstraintValidator<MinAmount, BigDecimal> {

    @Value("${services.loan.validation.amount.min}")
    private BigDecimal minimalAmount;

    @Override
    public boolean isValid(BigDecimal amount, ConstraintValidatorContext constraintValidatorContext) {
        return amount == null || amount.compareTo(minimalAmount) >= 0;
    }

}

package pl.krzaczek.wojciech.service.loan.loan.service.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class LoanAlreadyProlongatedException extends RuntimeException {

    public LoanAlreadyProlongatedException(String msg) {
        super(msg);
        log.error(msg);
    }
}

--liquibase formatted sql

--changeset Wojciech Krzaczek:1
CREATE TABLE loan
(
id SERIAL PRIMARY KEY NOT NULL,
amount NUMERIC(1000, 2) NOT NULL,
repaymentAmount NUMERIC(1000, 2) NOT NULL,
duration INT NOT NULL,
prolongated BOOLEAN NOT NULL,
date TIMESTAMP NOT NULL
);

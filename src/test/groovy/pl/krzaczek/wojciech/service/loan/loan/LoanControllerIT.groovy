package pl.krzaczek.wojciech.service.loan.loan

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import pl.krzaczek.wojciech.service.loan.loan.model.Loan
import pl.krzaczek.wojciech.service.loan.loan.service.LoanRepository
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static pl.krzaczek.wojciech.service.loan.common.api.ApiVersions.V1_JSON

@SpringBootTest
@ActiveProfiles('integration')
class LoanControllerIT extends Specification {

    MockMvc mockMvc
    def jsonSlurper = new JsonSlurper()

    @Autowired
    WebApplicationContext webApplicationContext

    @Autowired
    LoanRepository loanRepository

    @Autowired
    ObjectMapper objectMapper

    void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()
    }

    def 'should create loan'() {
        given:
            def request = [
                    amount  : 10000,
                    duration: 20,
                    date    : LocalDate.of(2300, 12, 10),
            ]
        when:
            def response = mockMvc.perform(post(URI.create('/api/loan'))
                    .contentType(V1_JSON)
                    .content(objectMapper.writeValueAsString(request))).andReturn()
                    .getResponse()
        then:
            response.status == HttpStatus.CREATED.value()
            String loanLocation = response.getHeader('Location')
            def id = Long.parseLong(loanLocation.substring(loanLocation.lastIndexOf('/') + 1))
            def loan = loanRepository.findById(id).get()
            loan.id == id
            loan.amount == request.amount
            loan.repaymentAmount == request.amount * 1.07
            loan.duration == request.duration
            !loan.prolongated
            loan.date == request.date
    }

    @Unroll
    def 'should not create loan if request contains violation'() {
        given:
            def request = [
                    amount  : amount,
                    duration: duration,
                    date    : date,
            ]
        when:
            def response = mockMvc.perform(post(URI.create('/api/loan'))
                    .contentType(V1_JSON)
                    .content(objectMapper.writeValueAsString(request))).andReturn()
                    .getResponse()
        then:
            response.status == HttpStatus.BAD_REQUEST.value()
            def responseJson = jsonSlurper.parseText(response.getContentAsString())
            responseJson.message.size() == 1
            responseJson.message.contains([key: violcationKey, reason: violcationReason])
        where:
            amount   | duration | date                       || violcationKey | violcationReason
            null     | 20       | LocalDate.of(2300, 12, 10) || 'amount'      | 'REQUIRED'
            10       | 20       | LocalDate.of(2300, 12, 10) || 'amount'      | 'TOO_LOW'
            10000000 | 20       | LocalDate.of(2300, 12, 10) || 'amount'      | 'TOO_BIG'
            10000    | null     | LocalDate.of(2300, 12, 10) || 'duration'    | 'REQUIRED'
            10000    | 10       | LocalDate.of(2300, 12, 10) || 'duration'    | 'TOO_LOW'
            10000    | 100      | LocalDate.of(2300, 12, 10) || 'duration'    | 'TOO_BIG'
            10000    | 20       | null                       || 'date'        | 'REQUIRED'
            10000    | 20       | LocalDate.of(2000, 12, 10) || 'date'        | 'PAST_DATE'


    }

    def 'should prolonage loan'() {
        given:
            def loan = loanRepository.save(new Loan(BigDecimal.valueOf(1000), BigDecimal.valueOf(1007), 20, LocalDate.now()))
        when:
            def response = mockMvc.perform(put(URI.create("/api/loan/${loan.id}/prolongation"))
                    .accept(V1_JSON))
                    .andReturn()
                    .getResponse()
        then:
            response.status == HttpStatus.OK.value()
            def expectedLoan = loanRepository.findById(loan.id).get()
            expectedLoan.id == loan.id
            expectedLoan.amount == loan.amount
            expectedLoan.repaymentAmount == loan.repaymentAmount
            expectedLoan.duration == loan.duration + 6
            expectedLoan.prolongated
            expectedLoan.date == loan.date
    }

    def 'should reutnr 400 if already prolongated loan'() {

        given:
            def loan = new Loan(BigDecimal.valueOf(1000), BigDecimal.valueOf(1007), 20, LocalDate.now())
            loan.prolongated = true
            loan = loanRepository.save(loan)
        when:
            def response = mockMvc.perform(put(URI.create("/api/loan/${loan.id}/prolongation"))
                    .accept(V1_JSON))
                    .andReturn()
                    .getResponse()
        then:
            response.status == HttpStatus.BAD_REQUEST.value()
    }

    def 'should return 404 if no loan '() {
        when:
            def response = mockMvc.perform(put(URI.create("/api/loan/999/prolongation"))
                    .accept(V1_JSON))
                    .andReturn()
                    .getResponse()
        then:
            response.status == HttpStatus.NOT_FOUND.value()
    }
}

package pl.krzaczek.wojciech.service.loan.loan.dto.validation

import spock.lang.Specification
import spock.lang.Unroll

class MinAmountValidatorTest extends Specification {

    def MINIMAL_AMOUNT = 10000
    def instance = new MinAmountValidator(minimalAmount: MINIMAL_AMOUNT)

    def 'should be valid when amount is null'() {
        given:
            def duration = null

        expect:
            instance.isValid(duration, null)
    }

    def 'should not be valid when amount less than minimal'() {
        given:
            def duration = BigDecimal.valueOf(9999)

        expect:
            !instance.isValid(duration, null)
    }

    @Unroll
    def 'should be valid when amount equal or greater than minimal'() {
        expect:
            instance.isValid(duration, null)

        where:
            duration << [BigDecimal.valueOf(100000), BigDecimal.valueOf(100001)]
    }
}

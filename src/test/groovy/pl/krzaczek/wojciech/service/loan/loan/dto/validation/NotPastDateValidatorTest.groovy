package pl.krzaczek.wojciech.service.loan.loan.dto.validation

import pl.krzaczek.wojciech.service.loan.common.service.CurrentTimeProvider
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate

class NotPastDateValidatorTest extends Specification {

    def currentTimeProvider = Mock(CurrentTimeProvider)
    def instance = new NotPastDateValidator(currentTimeProvider)

    def 'should be valid when given date is null'() {
        given:
            def localDate = null

        expect:
            instance.isValid(localDate, null)
    }

    @Unroll
    def 'should be valid when given date is not past'() {
        when:
            def valid = instance.isValid(localDate, null)

        then:
            1 * currentTimeProvider.getLocalDate() >> LocalDate.now()
            valid

        where:
            localDate << [LocalDate.now(), LocalDate.now().plusDays(1)]
    }

    def 'should not be valid when given date is past'() {
        given:
            def localDate = LocalDate.now().minusDays(1)

        when:
            def valid = instance.isValid(localDate, null)

        then:
            1 * currentTimeProvider.getLocalDate() >> LocalDate.now()
            !valid
    }

}

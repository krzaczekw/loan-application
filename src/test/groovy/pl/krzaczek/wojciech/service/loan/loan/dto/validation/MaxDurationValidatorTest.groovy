package pl.krzaczek.wojciech.service.loan.loan.dto.validation

import spock.lang.Specification
import spock.lang.Unroll

class MaxDurationValidatorTest extends Specification {

    def MAXIMAL_DURATION = 10
    def instance = new MaxDurationValidator(maximalDuration: MAXIMAL_DURATION)

    def 'should be valid when duration is null'() {
        given:
            def duration = null

        expect:
            instance.isValid(duration, null)
    }

    def 'should not be valid when duration greater than maximal'() {
        given:
            def duration = 11

        expect:
            !instance.isValid(duration, null)
    }

    @Unroll
    def 'should be valid when duration equal or less than maximal'() {
        expect:
            instance.isValid(duration, null)

        where:
            duration << [9, 10]
    }
}

package pl.krzaczek.wojciech.service.loan.loan.dto.validation

import spock.lang.Specification
import spock.lang.Unroll

class MinDurationValidatorTest extends Specification {

    def MINIMAL_DURATION = 10
    def instance = new MinDurationValidator(minimalDuration: MINIMAL_DURATION)

    def 'should be valid when duration is null'() {
        given:
            def duration = null

        expect:
            instance.isValid(duration, null)
    }

    def 'should not be valid when duration less than minimal'() {
        given:
            def duration = 9

        expect:
            !instance.isValid(duration, null)
    }

    @Unroll
    def 'should be valid when duration equal or greater than minimal'() {
        expect:
            instance.isValid(duration, null)

        where:
            duration << [10, 11]
    }
}

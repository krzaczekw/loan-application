package pl.krzaczek.wojciech.service.loan.loan.dto.validation

import pl.krzaczek.wojciech.service.loan.common.service.CurrentTimeProvider
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalTime

class MaxAmountValidatorTest extends Specification {

    def MAX_INVALIDATION_FROM_HOUR = 1
    def MAX_INVALIDATION_TO_HOUR = 2
    def MAXIMAL_AMOUNT = 10000
    def currentTimeProvider = Mock(CurrentTimeProvider)

    def instance

    void setup() {
        instance = new MaxAmountValidator(currentTimeProvider)
        instance.maximalAmount = MAXIMAL_AMOUNT
        instance.maxInvalidationFromHour = MAX_INVALIDATION_FROM_HOUR
        instance.maxInvalidationToHour = MAX_INVALIDATION_TO_HOUR
    }

    def 'should be valid when amount is null'() {
        given:
            def amount = null

        when:
            def valid = instance.isValid(amount, null)
        then:
            valid
            0 * currentTimeProvider.getLocalTime()
    }

    def 'should be valid when amount less than maximal'() {
        given:
            def duration = 9999

        when:
            def valid = instance.isValid(duration, null)
        then:
            valid
            0 * currentTimeProvider.getLocalTime()
    }

    def 'should not be valid when amount greater than maximal'() {
        given:
            def duration = 10001

        when:
            def valid = instance.isValid(duration, null)
        then:
            !valid
            1 * currentTimeProvider.getLocalTime() >> LocalTime.now()
    }

    @Unroll
    def 'should be valid when amount greater equal maximal and time outside invalidation period'() {
        when:
            def valid = instance.isValid(MAXIMAL_AMOUNT, null)
        then:
            valid
            1 * currentTimeProvider.getLocalTime() >> time
        where:
            time << [LocalTime.of(2, 5), LocalTime.of(0, 59)]
    }

    @Unroll
    def 'should not be valid when amount greater equal maximal and time inside invalidation period'() {
        when:
            def valid = instance.isValid(MAXIMAL_AMOUNT, null)
        then:
            !valid
            1 * currentTimeProvider.getLocalTime() >> time
        where:
            time << [LocalTime.of(1, 59), LocalTime.of(1, 0)]
    }


}

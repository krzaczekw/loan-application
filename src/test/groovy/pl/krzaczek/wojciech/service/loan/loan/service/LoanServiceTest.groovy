package pl.krzaczek.wojciech.service.loan.loan.service

import pl.krzaczek.wojciech.service.loan.loan.dto.LoanRequest
import pl.krzaczek.wojciech.service.loan.loan.model.Loan
import pl.krzaczek.wojciech.service.loan.loan.service.exception.LoanAlreadyProlongatedException
import pl.krzaczek.wojciech.service.loan.loan.service.exception.LoanNotFoundException
import spock.lang.Specification

import java.time.LocalDate

class LoanServiceTest extends Specification {

    def instance

    def loanRepository = Mock(LoanRepository)
    def loanFactory = Mock(LoanFactory)

    def PROLONGATION = 6;
    def INTEREST = 0.07

    void setup() {
        instance = new LoanService(loanRepository, loanFactory)
        instance.prolongation = PROLONGATION
        instance.interest = INTEREST
    }

    def 'should create loan and calculate interest amount'() {
        given:
            def loanRequest = new LoanRequest(BigDecimal.valueOf(1000), 40, LocalDate.now())
            def repaymentAmount = BigDecimal.valueOf(1070)
            def loan = new Loan(id: 2L)
        when:
            def loanId = instance.createLoan(loanRequest)
        then:
            loanId == 2L
            1 * loanFactory.create(loanRequest, repaymentAmount) >> loan
            1 * loanRepository.save(loan) >> loan
    }

    def 'should prolongate loan'() {
        given:
            def id = 7
            def loan = new Loan(id: 7, date: LocalDate.of(2020, 10, 10), prolongated: false, duration: 6)
        when:
            def loanDeadline = instance.prolongateLoan(id)
        then:
            loanDeadline == LocalDate.of(2020, 10, 22)
            1 * loanRepository.findById(id) >> Optional.of(loan)
            1 * loanRepository.save(new Loan(id: 7, date: LocalDate.of(2020, 10, 10), prolongated: true, duration: 12))
    }

    def 'should not prolongate if loan not found'() {
        given:

        when:
            instance.prolongateLoan(7)
        then:
            thrown(LoanNotFoundException)
            1 * loanRepository.findById(7) >> Optional.empty()
            0 * loanRepository.save(_)
    }

    def 'should not prolongate if already prolongated'() {
        given:
            def id = 7
            def loan = new Loan(id: 7, date: LocalDate.of(2020, 10, 10), prolongated: true, duration: 6)
        when:
            instance.prolongateLoan(id)
        then:
            thrown(LoanAlreadyProlongatedException)
            1 * loanRepository.findById(id) >> Optional.of(loan)
            0 * loanRepository.save(_)
    }
}

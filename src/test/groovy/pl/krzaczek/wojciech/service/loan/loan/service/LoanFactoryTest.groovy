package pl.krzaczek.wojciech.service.loan.loan.service

import pl.krzaczek.wojciech.service.loan.loan.dto.LoanRequest
import spock.lang.Specification

import java.time.LocalDate

class LoanFactoryTest extends Specification {

    def instance = new LoanFactory()

    def 'should create loan'() {
        given:
            def loanRequest = new LoanRequest(BigDecimal.TEN, 12, LocalDate.now())
            def repaymentAmount = 1070.32
        when:
            def loan = instance.create(loanRequest, repaymentAmount)
        then:
            loan.id == null
            loan.amount == loanRequest.amount
            loan.repaymentAmount == repaymentAmount
            loan.duration == loanRequest.duration
            !loan.prolongated
            loan.date == loanRequest.date

    }
}
